FROM golang:alpine as build

WORKDIR /app

RUN go mod init app
COPY main.go .
RUN go build -o main .


FROM alpine

WORKDIR /app
RUN mkdir file
COPY --from=build /app/main .

CMD ["./main"]