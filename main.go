package main

import (
	"os"
	"fmt"
	"io/ioutil"
)


func main() {
	file 	 := os.Getenv("FILE")
	commands := loadInstructionsFromFile(file)
	message  := buildMessageFromInstructions(commands)

	fmt.Printf("%s\n", message)
}


func loadInstructionsFromFile(file string) []rune {
	content, err := ioutil.ReadFile("/app/file/" + file)
	if err != nil {
		fmt.Printf("Error al abrir el archivo: %v", err)
		os.Exit(1)
	}
	commands := []rune(string(content))
	return commands
}


func buildMessageFromInstructions(commands []rune) string {
	var decodedMessage string

	mapPunchs := builPunchMapFromCommands(commands)
	pointer   := 0
	message   := make([]rune, len(commands))

	for i := 0; i < len(commands); i++ {
		switch commands[i] {
			case '👉':
				pointer++
			case '👈':
				pointer--
			case '👆':
				if message[pointer] == 255 {
					message[pointer] = 0
				} else {
					message[pointer]++
				}
			case '👇':
				if message[pointer] == 0 {
					message[pointer] = 255
				} else { 
					message[pointer]--
				}
			case '🤜':
				if message[pointer] == 0 {
					i = mapPunchs[i]
				}
			case '🤛':
				if message[pointer] != 0 {
					i = mapPunchs[i]
				}
			case '👊':
				decodedMessage += string(message[pointer])
			default:
				// No hacer nada para otros caracteres
		}
	}
	return decodedMessage 
}


func builPunchMapFromCommands(commands []rune) map[int]int {
	var stack []int
	var punchLeftIndex int

	punchsMap := make(map[int]int)

	// Nota: 
	// La idea es armar un map con la siguiente info por cada par de '🤜' y '🤛'
	// mapPunchs[ indice('🤜') ] = indice('🤛')
	// mapPunchs[ indice('🤛') ] = indice('🤜')
	for index, value := range commands {
		if value == '🤜' {
			stack = append(stack, index) // punchLeftIndex
		} else if value == '🤛' {
			punchLeftIndex			  = stack[len(stack) - 1]
			punchsMap[index] 		  = punchLeftIndex
			punchsMap[punchLeftIndex] = index
			stack = stack[:len(stack) - 1]
		}
	}
	return punchsMap
}